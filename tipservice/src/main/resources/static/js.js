function checkForm(){
    var total = document.getElementById("total").value;
    var validation = (total > 0);
    document.getElementById("submit").disabled = !validation;
}

var slider = document.getElementById("sat");
var output = document.getElementById("satValue");
output.innerHTML = slider.value;

slider.oninput = function(){
    output.innerHTML = this.value;
}


function cents(){
    var totalInput = document.getElementById("total").value;
    totalInputFormatted = parseFloat(totalInput).toFixed(2);
    document.getElementById("total").value = totalInputFormatted;
}
