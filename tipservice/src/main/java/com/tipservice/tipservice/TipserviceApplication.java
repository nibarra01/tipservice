package com.tipservice.tipservice;

import java.util.InputMismatchException;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication(scanBasePackages = {"com.tipservice"})
@ComponentScan("com.tipservice.tipservice.controller")

public class TipserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TipserviceApplication.class, args);
		Scanner sc = new Scanner(System.in);

		// Party
		System.out.println("Enter party size:");
		String partySt = sc.nextLine();
		if (intCheck(partySt, 'a').equals("F-")){
			System.out.println("invalid input recieved.");
			boolean wrong = false;
			do{
				System.out.println("\nEnter party size: (integer >= 1)");
				partySt = sc.nextLine();
				if (intCheck(partySt, 'a').equals("F-")){
					wrong = true;
					System.out.println("invalid input recieved again.");
				} else {
					wrong = false;
				}
			} while (wrong);
		} 
		int partyInt = Integer.parseInt(partySt);

		// Satisfaction
		System.out.println("\nEnter satisfaction on a scale of 1-10");
		String satSt = sc.nextLine();
		if (intCheck(satSt, 'b').equals("F-")){
			System.out.println("invalid input recieved");
			boolean no = false;
			do{
				System.out.println("\nEnter satisfaction on a scale of 1-10\n(1, 2, 3, 4, 5, 6, 7, 8, 9, 10):");
				satSt = sc.nextLine();
				if (intCheck(satSt, 'b').equals("F-")){
					no = true;
					System.out.println("invalid input recieved again.");
				} else {
					no = false;
				}
			} while(no);
		}
		int satInt = Integer.parseInt(satSt);

		// total
		// DecimalFormat dollar = new DecimalFormat("#.##");
		// dollar.setRoundingMode(RoundingMode.DOWN);
		System.out.println("Enter the total:");
		String totalSt = sc.nextLine();
		if (doubleCheck(totalSt).equals("F-")){
			System.out.println("invalid input recieved");
			boolean stop = false;
			do {
				System.out.println("\nEnter the total:");
				totalSt = sc.nextLine();
				if (doubleCheck(totalSt).equals("F-")){
					stop = true;
					System.out.println("invaliud input recieved again.");
				} else {
					stop = false;
				}
			} while(stop);
		}
		// double totalDbl = Double.parseDouble(dollar.format(totalSt)); 
		double totalDbl = Double.parseDouble(totalSt);


		double tip;
		// calculations
		if (partyInt >= 6){
			switch (satInt){
				case 10:
				case 9:
				case 8:
				case 7:
				// default:
				tip = totalDbl * .25;
				System.out.printf("Tip for $%.2f and party size of %d with satisfaction of %d is $%.2f", totalDbl, partyInt, satInt, tip);
				break;
				default:
				tip = totalDbl * .2;
				System.out.printf("Tip for $%.2f and party size of %d with satisfaction of %d is $%.2f", totalDbl, partyInt, satInt, tip);
				break;
			}
		} else{
			switch (satInt){
				case 10:
				case 9:
				tip = totalDbl * .25;
				System.out.printf("Tip for $%.2f and party size of %d with satisfaction of %d is $%.2f", totalDbl, partyInt, satInt, tip);
				break;

				case 8:
				tip = totalDbl * .2;
				System.out.printf("Tip for $%.2f and party size of %d with satisfaction of %d is $%.2f", totalDbl, partyInt, satInt, tip);
				break;

				case 7:
				tip = totalDbl * .15;
				System.out.printf("Tip for $%.2f and party size of %d with satisfaction of %d is $%.2f", totalDbl, partyInt, satInt, tip);
				break;

				default:
				tip = totalDbl * 0;
				System.out.printf("Tip for $%.2f and satisfaction of %d is $%.2f", totalDbl, satInt, tip);
				break;
			}
		}
		System.out.println("\n\n");
		System.exit(0);
	}

	// validation method
	private static String intCheck(String test ,char type) {
		// is it an int
        try{
            Integer.parseInt(test);
        }
        catch (InputMismatchException | NumberFormatException e){
            return "F-";
		}

		// switch
		// a = is it positive
		// b = is it in the requested range
		switch (type){
			case 'a':
			if (type == 'a'){
				if (Integer.parseInt(test) <= 0){
					return "F-";
				} else{
					return test;
				}
			}
			break;
			case 'b':
			if (type == 'b'){
				if(Integer.parseInt(test) > 0 && Integer.parseInt(test) < 11){
					return test;
				} else {
					return "F-";
				}
			}
			break;
			default:
			System.out.println("an error has occured in intCheck");
			return "F-";

		}
		// the default should catch everything else why is do I need this
		System.out.println("an error has occured in intCheck \nThis line should never be reached");
		return "F-";

    }


    private static String doubleCheck(String test){
        try{
            Double.parseDouble(test);
        }
        catch (InputMismatchException | NumberFormatException e){
            return "F-";
		}
		
		if(Double.parseDouble(test) <= 0){
			return "F-";
		}
        return test;
    }

}
