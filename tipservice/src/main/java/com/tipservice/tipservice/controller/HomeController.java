package com.tipservice.tipservice.controller;

import java.text.DecimalFormat;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
    @GetMapping("/")
	public String plusTip (Model m){

        int party = 0;
        double total = 0.00;
        int sat = 5;
        m.addAttribute("party", party);
        m.addAttribute("total", total);
        m.addAttribute("sat", sat);

		return "tipper";
    }
    
    @PostMapping("/tip")
    public String tipping (Model m, @RequestParam(name="party") int party, @RequestParam(name="total") double total, @RequestParam(name="sat") int sat){
        double tip;
        if (party <= 5) {
            switch(sat){
                case 10:
				case 9:
				tip = total * .25;
				break;

				case 8:
				tip = total * .2;
				break;

				case 7:
				tip = total * .15;
				break;

				default:
				tip = total * 0;
				break;

            }
        } else {
            switch(sat){
                case 10:
				case 9:
				case 8:
				case 7:
				tip = total * .25;
                break;
                
				default:
				tip = total * .2;
            }
        }


        total = Math.floor(total * 100)/100;
        tip = Math.floor(tip * 100)/100;
        DecimalFormat cents = new DecimalFormat("#.00");
        String totalFmt = cents.format(total);
        String tipFmt = cents.format(tip);
        double fullTotal = total + tip;
        String ftFmt = cents.format(fullTotal);

        m.addAttribute("party", party);
        m.addAttribute("sat", sat);
        m.addAttribute("total", totalFmt);
        m.addAttribute("tip", tipFmt);
        m.addAttribute("final", ftFmt);

        return "tipped";
    }

    // @GetMapping("/tip")
    // public String tipped(){
    //     return "tipped";
    // }
}

